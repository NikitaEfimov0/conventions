# Conventions

Репозиторий относится к 3 дню марафона по Android-разработке. Решения заданий модуля 3 (Conventions):
1) [Comparison](https://gitlab.com/NikitaEfimov0/conventions/-/blob/main/comparison.kt)
2) [Ranges](https://gitlab.com/NikitaEfimov0/conventions/-/blob/main/ranges.kt)
3) [For loop](https://gitlab.com/NikitaEfimov0/conventions/-/blob/main/for_loop.kt)
4) [Operator overloading](https://gitlab.com/NikitaEfimov0/conventions/-/blob/main/operators_overloading.kt)
5) [Invoke](https://gitlab.com/NikitaEfimov0/conventions/-/blob/main/invoke.kt)

[Скриншот вердикта проверяющей системы](https://gitlab.com/NikitaEfimov0/conventions/-/blob/main/proof.png)
