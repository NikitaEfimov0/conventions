data class MyDate(val year: Int, val month: Int, val dayOfMonth: Int) : Comparable<MyDate> {
    override fun compareTo(date:MyDate):Int{
        if(this.year >= date.year && this.month>=date.month && this.dayOfMonth >= date.dayOfMonth){
            return 1
        }
        return -1
    }
}

fun test(date1: MyDate, date2: MyDate) {
    // this code should compile:
    println(date1 < date2)
}
