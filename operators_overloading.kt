import TimeInterval.*

data class MyDate(val year: Int, val month: Int, val dayOfMonth: Int)

operator fun MyDate.plus(timeInterval:TimeInterval) = addTimeIntervals(timeInterval, 1)
// Supported intervals that might be added to dates:

class RepeatedTimeInterval(val timeInterval:TimeInterval, val number:Int)
enum class TimeInterval { DAY, WEEK, YEAR }
operator fun TimeInterval.times(number: Int) =
        RepeatedTimeInterval(this, number)

operator fun MyDate.plus(timeIntervals: RepeatedTimeInterval) =
        addTimeIntervals(timeIntervals.timeInterval, timeIntervals.number)

fun task1(today: MyDate): MyDate {
    return today + YEAR + WEEK
}

fun task2(today: MyDate): MyDate {
    return today + YEAR * 2 + WEEK * 3 + DAY * 5
}
